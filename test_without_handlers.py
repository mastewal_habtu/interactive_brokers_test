from ib.opt import Connection, message

from ib.ext.Contract import Contract
from ib.ext.Order import Order

def error_handler(msg):
    """Handles the capturing of error messages"""
    print("Server Error: %s" % msg)

def reply_handler(msg):
    """Handles of server replies"""
    print("Server Response: %s, %s" % (msg.typeName, msg))

def make_contract(symbol, sec_type, exch, prim_exchange, curr):
    Contract.m_symbol = symbol
    Contract.m_secType = sec_type
    Contract.m_exchange = exch
    Contract.m_primaryExch = prim_exchange
    Contract.m_currency = curr

    return Contract

def make_order(action, quantity, price=None):
    if price is not None:
        order = Order()
        order.m_orderType = 'LMT'
        order.m_totalQuantity = quantity
        order.m_action = action
        order.m_lmtPrice = price
    else:
        order = Order()
        order.m_orderType = 'MKT'
        order.m_totalQuantity = quantity
        order.m_action = action

    return order

cid = 303
def main():
    print("Started connecting ...")
    conn = Connection.create(port=7497, clientId=999)
    conn.connect()

    conn.register(error_handler, 'Error')
    conn.registerAll(reply_handler)

    cont = make_contract('TSLA', 'STK', 'SMART', 'SMART', 'USD')
    offer = make_order('BUY', 1, 200)

    oid = cid

    conn.placeOrder(oid, cont, offer)

    conn.disconnect()

if __name__ == '__main__':
    main()